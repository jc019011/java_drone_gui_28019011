package drone_gui;
import java.util.Random;

public class Direction {
    public Integer x_move;
    public Integer y_move;


    Direction(){
        Random random = new Random();
        x_move = random.nextInt(20 + 20)-20;
        y_move = random.nextInt(20 + 20)-20;
    }

    public void flipDireaction(){
        x_move = -x_move;
        y_move = -y_move;
    }
}
