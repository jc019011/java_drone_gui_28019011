package drone_gui;

public abstract class Drone extends Entity{

    private Integer speed;
    private Direction dir;

    Drone(int x, int y, int s) {
        super(x, y);
        speed = s;
        dir = new Direction();
    }

    public Integer getSpeed() {
        return speed;
    }
    public void setSpeed(int s){
        speed = s;
    }

    public Direction getDir() {
        return dir;
    }

    public void setNewDir(){
        dir = new Direction();
    }

}
