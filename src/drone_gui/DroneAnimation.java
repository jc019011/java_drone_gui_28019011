package drone_gui;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class DroneAnimation extends Application {
    public int xCanvasSize = 800;
    public int yCanvasSize = 800;
    public EntityCanvas ec;
    public EntityWorld ew;
    boolean animation = false;



    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Drone Animation");
        BorderPane bp = new BorderPane();
        Group root = new Group();
        Canvas canvas = new Canvas(xCanvasSize, yCanvasSize);
        root.getChildren().add(canvas);
        ec = new EntityCanvas(canvas.getGraphicsContext2D(), xCanvasSize,yCanvasSize);
        ew = new EntityWorld(xCanvasSize,yCanvasSize);

        new AnimationTimer()
        {
            private long lastUpdate = 0;
            @Override
            public void handle(long currentNanoTime){
                if(animation && currentNanoTime - lastUpdate >= 100_000_000){
                    ew.updateWorld(ec);
                    ew.drawWorld(ec);
                    lastUpdate = currentNanoTime;
                }
            }
        }.start();

        bp.setCenter(root);

        bp.setTop(setMenu());
        bp.setBottom(setButtons());
        Scene scene = new Scene(bp, xCanvasSize*1.6, yCanvasSize*1.6);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void displaySys(){
        ew.updateWorld(ec);
        ew.drawWorld(ec);
    }

    private void loadFile() throws IOException{
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                JFileChooser fc = new JFileChooser("");
                int ap = fc.showOpenDialog(null);
                if(ap == JFileChooser.APPROVE_OPTION){
                    File f = fc.getSelectedFile();
                    if(f.isFile()){
                        try{
                            String path = f.getPath();
                            Path fp = Paths.get(path);
                            long lineCount = Files.lines(fp).count();
                            int x_pos = 0;
                            int y_pos = 0;
                            int xMove = 0;
                            int yMove = 0;
                            int speed = 0;
                            int entityNumber = 0;
                            String type = "";
                            int worldYSize = Integer.parseInt(Files.readAllLines(fp).get(0),10);
                            int worldXSize = Integer.parseInt(Files.readAllLines(fp).get(1),10);
                            ew = new EntityWorld(worldXSize,worldYSize);
                            for(int x = 2; x<lineCount; x = x+7){
                                x_pos = Integer.parseInt(Files.readAllLines(fp).get(x),10);
                                y_pos = Integer.parseInt(Files.readAllLines(fp).get(x+1),10);
                                xMove = Integer.parseInt(Files.readAllLines(fp).get(x+2),10);
                                yMove = Integer.parseInt(Files.readAllLines(fp).get(x+3),10);
                                speed = Integer.parseInt(Files.readAllLines(fp).get(x+4),10);
                                entityNumber = Integer.parseInt(Files.readAllLines(fp).get(x+5),10);
                                type = Files.readAllLines(fp).get(x+6);
                                ew.addSpecificEntity(x_pos,y_pos,xMove,yMove,speed,entityNumber,type);
                            }
                            displaySys();
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        th.start();
    }

    private MenuBar setMenu(){
        Menu file = new Menu("File");
        MenuItem save = new MenuItem("Save");
        save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try{
                    ew.saveWorld();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        MenuItem load = new MenuItem("Load");
        load.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try{
                    loadFile();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.exit(0);
            }
        });
        file.getItems().addAll(save,load,exit);

        Menu entityControl = new Menu("EntityControl");
        MenuItem addBase = new MenuItem("Base");
        addBase.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                ew.addEntity("base");
                displaySys();
            }
        });
        MenuItem addPredator = new MenuItem("Predator");
        addPredator.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                ew.addEntity("predator");
                displaySys();
            }
        });
        MenuItem addObstacle = new MenuItem("Obstacle");
        addObstacle.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                ew.addEntity("obstacle");
                displaySys();
            }
        });
        entityControl.getItems().addAll(addBase,addPredator,addObstacle);

        MenuBar mb = new MenuBar();
        mb.getMenus().addAll(file,entityControl);
        return mb;
    }




    private HBox setButtons(){
        Button animateStart = new Button("Start Animation");
        animateStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                animation = true;
            }
        });

        Button animateStop = new Button("Stop Animation");
        animateStop.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                animation = false;
            }
        });

        return new HBox(animateStart, animateStop);
    }
}
