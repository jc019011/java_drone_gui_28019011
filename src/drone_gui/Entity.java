package drone_gui;


public abstract class Entity {

    public static Integer entities = 0;


    private Integer x_pos;
    private Integer y_pos;
    private int speed;

    private Integer entity_number;

    Entity(int x, int y){

        x_pos = x;
        y_pos = y;

        entity_number = entities;
        entities++;
    }

    public String writeInfo(){
        return "This entity has x_pos: " + x_pos + " y_pos: " + y_pos;
    }


    public Integer getX_pos() {
        return x_pos;
    }
    public Integer getY_pos() {
        return y_pos;
    }
    public Integer getEntityNumber(){
        return entity_number;
    }
    public void setEntityNumber(int entNo){
        entity_number = entNo;
    }

    public void setX_pos(int x){
        x_pos = x;
    }

    public void setY_pos(int y){
        y_pos = y;
    }

    public abstract void tryToMove(EntityWorld entityWorld);
    public abstract int getSize();
    public abstract void performAction(EntityWorld entityWorld, Entity e);

    protected abstract Direction getDir();

    public abstract String saveEntity();
}
