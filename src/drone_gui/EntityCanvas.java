package drone_gui;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.geometry.VPos;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

public class EntityCanvas {
    private GraphicsContext gc;
    private int xCanvasSize;
    private int yCanvasSize;

    EntityCanvas(GraphicsContext graphicsContext2D, int x, int y){
        gc = graphicsContext2D;
        xCanvasSize = x;
        yCanvasSize = y;
    }

    public double getXCanvasSize() {
        return xCanvasSize;
    }
    public double getYCanvasSize() {
        return yCanvasSize;
    }

    public void clearCanvas() {
        gc.clearRect(0,0,xCanvasSize,yCanvasSize);
    }

    public void fillCanvas(int width, int height){
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, width , height);
        gc.setStroke(Color.BLACK);
        gc.strokeRect(0, 0, width, height);
    }

    public void showCircle(int x , int y, int rad){
        gc.fillArc(x-rad, y-rad, rad*2, rad*2, 0, 360, ArcType.ROUND);
    }

    public void showEntities(EntityWorld ew){
        for(Entity e:ew.getEntityList()){
            if(e != null){
                showCircle(e.getX_pos(),e.getY_pos(),e.getSize());
            }
        }
    }

}
