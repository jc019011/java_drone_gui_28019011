package drone_gui;

import javax.swing.*;
import java.util.Random;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;


public class EntityWorld {
    private int worldSizeX;
    private int worldSizeY;
    private Entity[][] entityWorld;
    private Entity[] entityList;
    private static int numberOfEntities = 0;
    private Random randomGenerator;

    EntityWorld(int x, int y){
        worldSizeX = x;
        worldSizeY = y;
        entityWorld = new Entity[x][y];
        randomGenerator = new Random();
        entityList = new Entity[x*y];
    }


    public void addEntity(String type){
        Integer randomX;
        Integer randomY;

        do{
            randomY = randomGenerator.nextInt(worldSizeY);
            randomX = randomGenerator.nextInt(worldSizeX);
        } while(getEntityAt(randomX, randomY) != null || randomY <= 1 || randomX <= 1);

        switch(type){
            default:
            case "base":
                Base b = new Base(randomX,randomY);
                entityWorld[randomX][randomY]=b;
                entityList[numberOfEntities] = b;
                numberOfEntities++;
                break;
            case "predator":
                Predator p = new Predator(randomX,randomY);
                entityWorld[randomX][randomY]=p;
                entityList[numberOfEntities] = p;
                numberOfEntities++;
                break;
            case "obstacle":
                Obstacle o = new Obstacle(randomX,randomY);
                entityWorld[randomX][randomY] = o;
                entityList[numberOfEntities] = o;
                numberOfEntities++;
                break;
        }
    }

    public void addSpecificEntity(int x_pos, int y_pos, int xMove, int yMove, int speed, int entityNo, String type){
        switch (type){
            default:
            case "base":
                Base b = new Base(x_pos,y_pos);
                b.getDir().x_move = xMove;
                b.getDir().y_move = yMove;
                b.setEntityNumber(entityNo);
                b.setSpeed(speed);
                entityWorld[x_pos][y_pos]=b;
                entityList[entityNo] = b;
                numberOfEntities = entityNo;
                break;
            case "predator":
                Predator p = new Predator(x_pos,y_pos);
                p.getDir().x_move = xMove;
                p.getDir().y_move = yMove;
                p.setEntityNumber(entityNo);
                p.setSpeed(speed);
                entityWorld[x_pos][y_pos]=p;
                entityList[entityNo] = p;
                numberOfEntities = entityNo;
                break;
            case "obstacle":
                Obstacle o = new Obstacle(x_pos,y_pos);
                o.setEntityNumber(entityNo);
                entityWorld[x_pos][y_pos]=o;
                entityList[entityNo] = o;
                numberOfEntities = entityNo;
                break;
        }
    }

    public Entity getEntityAt(int x, int y){
        if(entityWorld[x][y] != null){
            return entityWorld[x][y];
        } else{
            return null;
        }
    }

    public boolean canMoveHere(int x, int y, Entity e){
        if(x < worldSizeX-e.getSize() && x > e.getSize() && y <worldSizeY-e.getSize() && y> e.getSize()){

            if(getEntityAt(x+e.getSize(),y+e.getSize()) == null ){
                return true;
            }else if(getEntityAt(x+e.getSize(),y+e.getSize())!= null){
                Entity moveE = getEntityAt(x+e.getSize(),y+e.getSize());
                moveE.performAction(this,e);
            }
        }
        return false;
    }


    public void adjustEntities(EntityCanvas ec){
        for(Entity e: entityList){
            if(e != null){
                e.tryToMove(this);
            }
        }
    }

    public void updateWorld(EntityCanvas ec){
        adjustEntities(ec);
    }

    public void drawWorld(EntityCanvas ec){
        ec.clearCanvas();
        ec.showEntities(this);
    }

    public Entity[] getEntityList(){
        return entityList;
    }

    public void removeEntity(Entity e) {
        entityWorld[e.getX_pos()][e.getY_pos()] = null;
        entityList[e.getEntityNumber()] = null;
    }

    private String saveWorldData() {
        String saveData = "";
        saveData += worldSizeY+"\n";
        saveData += worldSizeX;
        for(Entity e:entityList){
            if(e != null){
                saveData += "\n";
                saveData += e.saveEntity();

            }
        }
        return saveData;
    }

    public void saveWorld() throws IOException {
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                JFileChooser fc = new JFileChooser("");
                int ap = fc.showDialog(null,"Save");
                if(ap == JFileChooser.APPROVE_OPTION){
                    File f = fc.getSelectedFile();
                    try {
                        FileWriter writeF= new FileWriter(f);
                        PrintWriter writeH = new PrintWriter(writeF);
                        writeH.println(saveWorldData());
                        writeH.close();
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }
        });
        th.start();
    }
}
