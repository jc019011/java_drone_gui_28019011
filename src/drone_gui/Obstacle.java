package drone_gui;

public class Obstacle extends Entity{
    public int speed = 0;
    public int size = 15;

    Obstacle(int x, int y) {
        super(x, y);
    }

    public void tryToMove(EntityWorld entityWorld) {

    }


    public int getSize() {
        return size;
    }


    public void performAction(EntityWorld entityWorld, Entity e) {

    }


    protected Direction getDir() {
        return null;
    }


    public String saveEntity() {
        String res = "";
        res += getX_pos() + "\n";
        res += getY_pos() + "\n";
        res += "0\n";
        res += "0\n";
        res += "0\n";
        res += getEntityNumber() + "\n";
        res += "obstacle";
        return res;
    }
}
