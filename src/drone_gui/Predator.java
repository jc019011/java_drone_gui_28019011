package drone_gui;

public class Predator extends Drone {

    private static final Integer speed = 2;
    private static final int size = 20;
    Predator(int x, int y){
        super(x, y, speed);
    }

    public void tryToMove(EntityWorld ew){
        int newX = getX_pos() + (speed * getDir().x_move);
        int newY = getY_pos() + (speed * getDir().y_move);



        if(ew.canMoveHere(newX,newY,this)){
            setX_pos(newX);
            setY_pos(newY);
        }
        setNewDir();
    }
    public int getSize(){
        return size;
    }


    public void performAction(EntityWorld entityWorld, Entity e) {
        if(e instanceof Predator){
            entityWorld.removeEntity(this);
        }else if(e instanceof Base){
            this.getDir().flipDireaction();
        }else if(e instanceof Obstacle){
            this.getDir().flipDireaction();
        }
    }


    public String saveEntity() {
        String res = "";
        res += getX_pos() + "\n";
        res += getY_pos() + "\n";
        res += getDir().x_move +"\n";
        res += getDir().y_move +"\n";
        res += getSpeed() + "\n";
        res += getEntityNumber() + "\n";
        res += "predator";
        return res;
    }
}
